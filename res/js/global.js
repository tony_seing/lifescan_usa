$(document).ready(function() {
  	
  $('body').bind('touchmove', function (e) {
    e.preventDefault();
    return true; 
  });	
  
  
  $(".coverage-btn, #coverage-btn").click(function() {
    window.location.href = "../coverage/coverage.html";
  });

  $(".coverage-btn-fromhome").click(function() {
    window.location.href = "coverage/coverage.html";
  });

  $(".coverage-btn-root").click(function() {
    window.location.href = "coverage/coverage.html";
  });

  $(".home-btn-left-root").click(function() {
    window.location.href = "lifescan_usa.html";
  });

  
  $("#btn-home, .home-btn-left, #home-btn-left, .btn-home").click(function() {
    window.location.href = "../lifescan_usa.html?visited=1";
  });
  

  $(".ultrameters-btn").click(function() {
    window.location.href = "ultrameters.html";
  });

  if (location.pathname.indexOf("lifescan_usa.html") != -1)
  {
    $('body').css('opacity', '0').fadeTo(1900, 1,'swing'); 
    
  }
  
});

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  results = regex.exec(location.search);
  return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

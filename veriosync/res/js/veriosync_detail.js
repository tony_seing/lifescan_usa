$(document).ready(function() {
  $("*").hammer().on("swiperight", function() {
    window.location.href = "../lifescan_usa.html?visited=1";
  });

  $("*").hammer().on("swipeleft", function() {
    window.location.href = "veriosync_close.html";
  });
  

  

  $(".easytouse").click(function() {

    $(".leftdot, .rightdot").show();
    $(".easytouse_message").show();
    $(".leftdot").attr("src", "res/images/darkdot.png");
    $(".tinybloodsample").show();


    clearButtons();
    clearChart();
    hideMessages();
    hideOnScreen();
    unilluminate();
    $(".easytouse").attr("src", "res/images/easy_to_use_blue.png");
    $(".mainchip").hide();
    $(".disclaimer").hide();
    $(".easytouse_message").show();
  


        
    $(this).attr("src", "res/images/easy_to_use_blue.png");
  });

  
  
  $(".easytosync").click(function() {
    clearButtons();
    hideMessages();
    hideOnScreen();
    unilluminate();

    $(".easytosync_message").show();
    
    $(this).attr("src", "res/images/easytosync_blue.png");
  });




  $(".simpleapp").click(function() {
    clearButtons();
    clearChart();
    $(".disclaimer").hide();
    hideMessages();
    unilluminate();
    hideOnScreen();
    $(".simpleapp_message").show();
    $(".mainchip").hide();

    $(this).attr("src", "res/images/simpleapp_blue.png");

  });
  

  $(".simple_app_0").click(function() {
    $(".simpleapp_message").attr("src", "res/images/simpleapp_0.png");
  });
    $(".simple_app_1").click(function() {
    $(".simpleapp_message").attr("src", "res/images/simpleapp_1.png");
  });
  $(".simple_app_2").click(function() {
    $(".simpleapp_message").attr("src", "res/images/simpleapp_2.png");
  });


  $(".accuracy").click(function() {
    clearButtons();
    clearChart();

    $(".disclaimer").hide();
    hideMessages();
    unilluminate();
    hideOnScreen();
    $(".accuracy_message").show();
    $("#learnmore").show();

    $(this).attr("src", "res/images/accuracy_blue.png");
  });

  
  $(".easytouse_circle_1").click(function() {
    clearButtons();
    clearChart();
    hideMessages();
    hideOnScreen();
    unilluminate();
    $(".easytouse_chip").attr("src", "res/images/easytouse_message_1.png");
    $(".easytouse").attr("src", "res/images/easy_to_use_blue.png");
    $("#Image-Maps-Com-image-maps-2015-01-13-145801").attr("src", "res/images/twodots_1.png");
    $(".mainchip").hide();
    $(".disclaimer").hide();
    $(".easytouse_message").show();
    
     return false;
  });
 
   $(".easytouse_circle_2").click(function() {
     $(".easytouse_chip").attr("src", "res/images/easytouse_message_2.png");
     $("body").css("background-image", " url('res/images/veriosync_illuminated_bkgd.png')");
     $(".checkresults").attr("src", "res/images/checkresults_illuminated.png");
     $(".header, .logo").hide();
     $(".easytouse").attr("src", "res/images/easy_to_use_blue.png");
     $("#Image-Maps-Com-image-maps-2015-01-13-145801").attr("src", "res/images/twodots_2.png");
return false;
  });
 
 
});



function clearButtons () {
  $(".easytouse").attr("src", "res/images/easytouse.png");
  $(".easytosync").attr("src", "res/images/easytosync.png");
  $(".simpleapp").attr("src", "res/images/simpleapp.png");
  $(".accuracy").attr("src", "res/images/accuracy.png");
  $(".leftdot, .rightdot").css("display", "none");
}

function clearChart() {
  TweenMax.to($(".chart"), 0.2, {autoAlpha:0});
}

function unilluminate() {
  $("body").css("background-image", "url('../res/images/background.jpg')");
  $(".easytouse_chip").attr("src", "res/images/easytouse_message_1.png");
  $("#Image-Maps-Com-image-maps-2015-01-13-145801").show();
  $(".checkresults").attr("src", "res/images/checkresults.png");
  $(".header").show();
  $(".logo").show();
}

function hideMessages() {
  $(".message").hide();
}

function showOnScreen() {
  $(".onscreenmessage").show();
}

function hideOnScreen() {
  $(".onscreenmessage").hide();
}


var screenimages = ["res/images/screen2.png", "res/images/screen1.png", "res/images/screen3.png"];

$(document).ready(function() {
  $("*").hammer().on("swiperight", function() {
    window.location.href = "lifescan_usa.html?visited=1";
  });
  
  $("*").hammer().on("swipeleft", function() {
    window.location.href = "verio_close.html";
  });

  $(".dot").click(dotClick);
  
  $("#learnmorelink").click(function() {
    $("#accuracy_popup").show();
//    hideMessages();
    $(".accuracy_message").show();
    return false;
  });
  
  $(".closepopup").click(function() {
    $("#accuracy_popup").hide();
    return false;
  });

  showChart();


  function showMeter() {
    $(".nopin").hide();
    $(".pin").hide();
    $(".veriometer").show();
  }

  $(".accuracy").click(function() {
    clearButtons();
    hideMessages();
    hideOnScreen();
    $("#learnmore, .accuracy_message").show();
    $(".justanumber").hide();
    $(this).attr("src", "res/images/accuracy_verio_selected.png");

  });

  
  $(".easytouse, .veriometer").click(function() {
    clearButtons();
    hideMessages();
    hideOnScreen();
    $(".nopin, .justanumber").show();
    $(".veriometer").hide();
    $(".easytouse_message").show();
    
    // tony seing's whacky tween stuff
    // ------------
    $(".pin").show();
    $('.pin').tween({
      left:{
	start: 565,
	stop: 565,
	time: 0,
	units: 'px',
	duration: 1,
	effect:'easeInOut'
      },
      top:{
	start: 50,
	stop: 216,
	time: 0,
	units: 'px',
	duration: 1,
	effect:'easeInOut'
      },
      onStop: function() {
	showMeter();
      }
    }).delay(0).play();
    
    // -------------

    $(".disclaimer").hide();
    $(".easytouse_message").show();
    
    $(".illteststrip").show();
    

    $(".easytouse").attr("src", "res/images/easytouse_selected_verio.png");
  });

  
  $(".colorrangeindicator").click(function() {
    clearButtons();
    hideMessages();
    hideOnScreen();


    $(".disclaimer").hide();
    $(".colorrangeindicator_message, .justanumber").show();

    
    $(this).attr("src", "res/images/colorrangeindicator_selected.png");
  });
  $(".dot1").click(function() {
    $("#verioscreen").show();
    $("#verioscreen").attr("src", screenimages[0]);
    $(".threedots").attr("src", "res/images/threedots_1.png");
  });
  

  $(".dot2").click(function() {
    $("#verioscreen").show();
    $("#verioscreen").attr("src", screenimages[1]);
        $(".threedots").attr("src", "res/images/threedots_2.png");
  });
  
  $(".dot3").click(function() {
    $("#verioscreen").show();
    $("#verioscreen").attr("src", screenimages[2]);
    $(".threedots").attr("src", "res/images/threedots_3.png");
  });

  $(".automaticmessages").click(function() {
    clearButtons();
    $(".disclaimer").hide();
    
    hideMessages();
    hideOnScreen();
    $(".makesresultssimple").show();
    $(".automaticmessages_message").show();
    $('#verioscreen').show();

    $(".threedots").show();
    $(this).attr("src", "res/images/automaticmessages_selected.png");
    
  });

  



});


function clearChart() {
  TweenMax.to($(".chart"), 0.2, {autoAlpha:0});
}


function showChart() {
  var pieOptions = {
    segmentShowStroke : true,
    segmentStrokeColor : "#2aaff8",
    segmentStrokeWidth : 6,
    animation : true,
    animationSteps : 30,
    animationEasing : "easeInOutSine",
    animateRotate : true,
    animateScale : false,
    onAnimationComplete : null
  };

  var pieData = [
    {
      value: 97,
      color:"#5bb4e5"
    },
    {
      value : 6,
      color : "#d6d6d6"
    }
  ];

  var myPie = new Chart(document.getElementById("chart").getContext("2d")).Pie(pieData, pieOptions);

}


function clearButtons () {
  $(".easytouse").attr("src", "res/images/easytouse_verio.png");
  $(".findshighsandlows").attr("src", "res/images/findshighsandlows.png");
  $(".automaticmessages").attr("src", "res/images/automaticmessages.png");
  $(".accuracy").attr("src", "res/images/accuracy_verio.png");
  // clears load text
  $(".97percent").hide();
  $(".correctsforthepresence").hide();
  $(".colorrangeindicator").attr("src", "res/images/colorrangeindicator.png");
  $(".makeresultssimple").hide();

}


function dotClick() {
  $(".dot").attr("src", "res/images/lightdot.png");
  $(this).attr("src", "res/images/darkdot.png");
}


function hideMessages() {
  $(".message").hide();
  
  $(".dot").hide();
  clearChart();
}

function showOnScreen() {
  $(".onscreenmessage").show();
}

function hideOnScreen() {
  $(".onscreenmessage").hide();
}


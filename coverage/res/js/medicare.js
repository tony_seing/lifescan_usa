$(document).ready(function() {
  
  $(".popup_btn").click(function() {
    $(".medicare-main-chip").hide();
    $(".threechoices").hide();
  });
  $(".copay_btn").click(function() {
    $(".copay_popup").show();
    $(".coveredat").hide();
  });
  $(".retail_btn").click(function() {
    $(".retail_popup, .ddd").show();
    $(".initial").hide();
    
  });
  $(".mail_btn").click(function() {
    $(".mail_popup").show();
    $(".initial").hide();
  });

  $(".close_popup").click(function() {
    $(".popup").hide();
    $(".initial").show();
    $(".ddd").hide();
    $(".medicare-main-chip").show();
    $(".threechoices").show();

  });


});

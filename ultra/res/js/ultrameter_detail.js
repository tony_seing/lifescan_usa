$(document).ready(function() {

  $(".easytouse_message").show();
  $(".easytouse").attr("src", "res/images/easy_to_use_blue_short.png");
  

  $(".easytouse").click(function() {
    clearButtons();
    hideMessages();
    hideOnScreen();
    $(".easytouse_message").show();
    unilluminate();
    $(this).attr("src", "res/images/easy_to_use_blue_short.png");
  });
  $(".mealflagging").click(function() {
    clearButtons();
    hideMessages();
    hideOnScreen();
    $(".mealflagging_message").show();
    $(this).attr("src", "res/images/meal_flagging_blue.png");
  });
  $(".trusted").click(function() {
    clearButtons();
    hideMessages();
    unilluminate();
    $(".trusted_message").show();
    showOnScreen();
    $(this).attr("src", "res/images/trusted_blue.png");
  });
  
  $(".popup_link").click(function() {
    $(".learnmore").show();
    return false;
  });
  
  $(".closepopup").click(function() {
    $(".learnmore").hide();
    return false;
  });

});

function clearButtons () {
  $(".easytouse").attr("src", "res/images/easy_to_use_grey_short.png");
  $(".mealflagging").attr("src", "res/images/meal_flagging_grey.png");
  $(".trusted").attr("src", "res/images/trusted_grey.png");
}

function unilluminate() {
  $("body").attr("class", "verioiq_background");
}

function hideMessages() {
  $(".message").hide();
}

function showOnScreen() {
  $(".onscreenmessage").show();
}

function hideOnScreen() {
  $(".onscreenmessage").hide();
}

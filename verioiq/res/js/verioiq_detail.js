$(document).ready(function() {
  $("*").hammer().on("swiperight", function() {
    window.location.href = "../lifescan_usa.html?visited=1";
  });

  $("*").hammer().on("swipeleft", function() {
    window.location.href = "verioiq_close.html";
  });
  
  $("#learnmorelink").click(function() {
    $("#accuracy_popup").show();
    return false;
  });
  
  $(".closepopup").click(function() {
    $("#accuracy_popup").hide();
    return false;
  });

  

  $(".easytouse").click(function() {
    clearButtons();
    clearChart();
    hideMessages();
    hideOnScreen();
    $(".leftdot, .rightdot").show();
    $(".disclaimer").hide();
    $(".easytouse_message").show();

    $(".colorlcdscreen").show();

    illuminate();
    $(this).attr("src", "res/images/easy_to_use_blue.png");
  });

  $(".rightdot").click(function() {
    clearChart();
    hideMessages();
    hideOnScreen();
    unilluminate();
    $(this).attr("src", "res/images/darkdot.png");
    $(".leftdot, .rightdot2").attr("src", "res/images/lightdot.png");
    $(".tinybloodsample").show();
    unilluminate();
  });
  
  
  $(".leftdot").click(function() {
    clearButtons();
    clearChart();
    hideMessages();
    hideOnScreen();
    
    $(".leftdot, .rightdot, .rightdot2").show();
    $(".disclaimer").hide();
    $(".easytouse_message").show();

    illuminate();
    $(".colorlcdscreen").show();

    $(".easytouse").attr("src", "res/images/easy_to_use_blue.png");
    
  });
  
  
  
  $(".findshighsandlows").click(function() {
    clearButtons();
    clearChart();
    hideMessages();
    hideOnScreen();
    unilluminate();
    
    $(".disclaimer").hide();
    $(".findshighsandlows_message").show();
    
    unilluminate();
    $(".onscreenmessage").show();
    $(this).attr("src", "res/images/findshighsandlows_selected.png");
  });




  $(".onesteptagging").click(function() {
    clearButtons();
    clearChart();
    $(".disclaimer").hide();
    hideMessages();
    unilluminate();
    hideOnScreen();
    $(".onesteptagging_message").show();
    $(".onesteptaggingscreen").show();
    $(this).attr("src", "res/images/onesteptagging_selected.png");

  });

  $(".accuracy").click(function() {
    clearButtons();
    clearChart();

    $(".disclaimer").hide();
    hideMessages();
    unilluminate();
    hideOnScreen();
    $("#learnmore").show();
    $(".accuracy_message").show();
    $(this).attr("src", "res/images/accuracy_selected.png");
  });


  showChart();
});

function showChart() {
  var pieOptions = {
    segmentShowStroke : true,
    segmentStrokeColor : "#2aaff8",
    segmentStrokeWidth : 6,
    animation : true,
    animationSteps : 30,
    animationEasing : "easeInOutSine",
    animateRotate : true,
    animateScale : false,
    onAnimationComplete : null
  };

  var pieData = [
    {
      value: 94,
      color:"#5bb4e5"
    },
    {
      value : 6,
      color : "#d6d6d6"
    }
  ];

  var myPie = new Chart(document.getElementById("chart").getContext("2d")).Pie(pieData, pieOptions);

}


function clearButtons () {
  $(".easytouse").attr("src", "res/images/easy_to_use_grey.png");
  $(".findshighsandlows").attr("src", "res/images/findshighsandlows.png");
  $(".onesteptagging").attr("src", "res/images/onesteptagging.png");
  $(".accuracy").attr("src", "res/images/accuracy.png");
  $(".leftdot, .rightdot, .rightdot2").css("display", "none");

}

function clearChart() {
  TweenMax.to($(".chart"), 0.2, {autoAlpha:0});
}

function unilluminate() {
  $("body").css("background-image", "url('res/images/iq_meter_white_bkgd.png')");
  $(".illuminated").hide();
  $(".unilluminated").show();
  $(".topleft").show();
}

function illuminate() {
  $("body").css("background-image", " url('res/images/verioiq_illuminated_bkgd.png')");
  $(".unilluminated").hide();
  $(".illuminated").show();
  
  $(".topleft").hide();
}


function hideMessages() {
  $(".message").hide();
    unilluminate();
}

function showOnScreen() {
  $(".onscreenmessage").show();
}

function hideOnScreen() {
  $(".onscreenmessage").hide();
}

